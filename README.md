# Megolodon: RSA Password Vault

---

This repo holds the config files needed to build a local docker version of my password manager.

## Installing

There are a few things that will need to happen of you want to run a local version of my password manager. Docker Secrets are used to keep the passwords for the MySQL database safe; before runing the `docker-compose up` command make sure you have added your passwords to the secrets database using `echo "your-root-password" | docker secret create mysql_root_password -` and `echo "your-user-password" | docker secret create mysql_user_password -`
